import React, {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { List, ListItem } from 'material-ui/List';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

import NewActivity from './newActivity';
import Dash from './dash';

class App extends Component {
  constructor(props) {
    super(props);

    this.startActivity = this.startActivity.bind(this);
    this.stopActivity = this.stopActivity.bind(this);
    this.fetchState = this.fetchState.bind(this);
    this.onStoreChanged = this.onStoreChanged.bind(this);

    this.state = { hasActivity: false }
  }

  componentDidMount() {
    this.fetchState();
    browser.storage.onChanged.addListener(this.onStoreChanged);
  }

  componentWillUnmount() {
    browser.storage.onChanged.removeListener(this.onStoreChanged);
  }

  onStoreChanged(storage) {
    const activity = {
      activityName: (storage.activityName) ? storage.activityName.newValue : this.state.activity.activityName,
      YES: (storage.YES) ? storage.YES.newValue : this.state.activity.YES,
      NO: (storage.NO) ? storage.NO.newValue : this.state.activity.NO,
    }

    this.setState({
      hasActivity: ! (storage.activityName && storage.activityName.oldValue && !storage.activityName.newValue),
      activity,
    });
  }

  fetchState() {
    browser.storage.local.get().then(res => {
      if (res.activityName) {
        this.setState({
          ...this.state,
          hasActivity: true,
          activity: res,
        });
      }
    });
  }

  startActivity(activityName, duration) {
    browser.runtime.sendMessage({
      type: 'START_ACTIVITY',
      name: activityName,
      duration,
    });
  }

  stopActivity() {
    browser.runtime.sendMessage({
      type: 'STOP_ACTIVITY',
      name: this.state.activity.activityName,
    });
  }

  render() {
    if (this.state.hasActivity) {
      return <Dash activity={this.state.activity} onActivityStop={this.stopActivity} />
    } else {
      return <NewActivity onNewActivity={this.startActivity} />
    }
  }
}

const AppWrapper = () => (
  <MuiThemeProvider>
    <App />
  </MuiThemeProvider>
)

export default AppWrapper;
