import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import { red500, red900, fullWhite } from 'material-ui/styles/colors';
import ThumbUp from 'material-ui/svg-icons/action/thumb-up';
import ThumbDown from 'material-ui/svg-icons/action/thumb-down';

const styles = {
  wrapper: {
    padding: '1rem 2rem',
  },
  h3: {
    margin: '0 0 .5rem 0',
    textAlign: 'center',
  },
  labelStyle: {
    fontWeight: 'bold',
    color: fullWhite,
  },
  horiz: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  vertAlign: {
    display: 'flex',
    alignItems: 'center',
  },
  marginLeft: {
    marginLeft: '.5rem',
    fontWeight: 'bold',
  }
}

class Dash extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }
  render() {
    return (
      <div style={styles.wrapper}>
        <h2 style={styles.h3}>You are {this.props.activity.activityName}ing</h2>
        <p style={styles.h3}>Keep focused!</p>
        <div style={styles.horiz}>
          <div style={styles.vertAlign}>
            <ThumbUp viewBox="0 0 24 24" />
            <div style={styles.marginLeft}>{this.props.activity.YES}</div>
          </div>
          <div style={styles.vertAlign}>
            <ThumbDown viewBox="0 0 24 24" />
            <div style={styles.marginLeft}>{this.props.activity.NO}</div>
          </div>
        </div>
        <br />
        <FlatButton label="Stop" style={styles.labelStyle} fullWidth backgroundColor={red500} hoverColor={red900} onClick={this.props.onActivityStop} />
      </div>
    )
  }
}

export default Dash;
