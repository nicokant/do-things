import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import { lightGreen500, lightGreen900, grey500, fullWhite } from 'material-ui/styles/colors';
import Slider from 'material-ui/Slider';

const styles = {
  wrapper: {
    padding: '1rem 2rem',
  },
  h3: {
    marginBottom: '.5rem',
    textAlign: 'center',
  },
  labelStyle(disabled = false) {
      return {
      fontWeight: 'bold',
      color: (disabled) ? grey500 : fullWhite,
    }
  },
  slider: {
    marginBottom: '.5rem',
  }
}

class NewActivity extends Component {
  constructor(props) {
    super(props);

    this.updatePeriod = this.updatePeriod.bind(this);
    this.updateText = this.updateText.bind(this);
    this.onClick = this.onClick.bind(this);

    this.state = { text: '', period: 5 }
  }

  updateText(e, val) {
    this.setState({
      ...this.state,
      text: val,
    });
  }

  updatePeriod(e, val) {
    this.setState({
      ...this.state,
      period: val,
    });
  }

  onClick() {
    this.props.onNewActivity(this.state.text, this.state.period);
  }

  render() {
    const disabled = this.state.text === "";
    const bgColor = (disabled) ? fullWhite : lightGreen500;
    const hColor = (disabled) ? fullWhite : lightGreen900;
    const labelStyle = styles.labelStyle(disabled);

    return (
      <div style={styles.wrapper}>
        <h2 style={styles.h3}>Activity Name</h2>
        <TextField hintText="Study" value={ this.state.text } onChange={this.updateText} fullWidth />
        <Slider step={1} min={1} max={10} value={this.state.period} sliderStyle={styles.slider} onChange={this.updatePeriod} />
        <p style={styles.h3}>Check each {this.state.period} mins</p>
        <FlatButton label="Start" labelStyle={labelStyle} fullWidth backgroundColor={bgColor} hoverColor={hColor} disabled={disabled} onClick={this.onClick} />
      </div>
    )
  }
}

export default NewActivity;
