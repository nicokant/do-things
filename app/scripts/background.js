const DELAY = 1;
const PERIOD = 1;

function startActivity({ name, duration }, sendResponse) {
  browser.storage.local.set({
    'activityName': name,
    'YES': 0,
    'NO': 0,
  }).then(() => {
    browser.alarms.create(`${name}-alarm`, {
      delayInMinutes: DELAY,
      periodInMinutes: duration,
    });
  }).catch(e => console.error(e));
}

function stopActivity({ name }) {
  browser.storage.local.clear();
  browser.alarms.clearAll();
  browser.notifications.clear(`${name}-notification`)
}

function handleMessage(request, sender, sendResponse) {
  switch (request.type) {
    case 'START_ACTIVITY':
      startActivity(request, sendResponse);
      break;

    case 'STOP_ACTIVITY':
      stopActivity(request)

    default:
      break;
  }

  return true;
}

function sendNotification(activityName) {
  browser.notifications.create(`${activityName}-notification`, {
    'type': 'basic',
    'iconUrl': browser.extension.getURL('images/icon-128.png'),
    'title': `Are you ${activityName}ing?`,
    'message': 'Try to keep yourself focused on the task!',
    buttons: [
      { title: 'YES' },
      { title: 'NO' },
    ]
  });
}

function handleAlarm(alarm) {
  console.log('got an alarm')
  browser.storage.local.get('activityName').then(({ activityName }) => {
    sendNotification(activityName);
  });
}

function handleClick(_, index) {
  const val = (index === 0) ? 'YES' : 'NO';
  browser.storage.local.get(val).then(res => {
    const next = {};
    next[val] = res[val] + 1;

    browser.storage.local.set(next);
  });
}

browser.runtime.onMessage.addListener(handleMessage);
browser.alarms.onAlarm.addListener(handleAlarm);
browser.notifications.onButtonClicked.addListener(handleClick);
